# ![Quatter](Resources/Textures/Logo.png)
A digital version of the [Quarto](https://en.wikipedia.org/wiki/Quarto_%28board_game%29) boardgame by Blaise Müller, made using the [Dry](https://gitlab.com/luckeyproductions/dry) game engine and other open source software.

![Screenshot](Screenshots/Screenshot_Mon_Nov_28_21_28_45_2022.png)

[![pipeline status](https://gitlab.com/luckeyproductions/games/Quatter/badges/master/pipeline.svg)](https://gitlab.com/luckeyproductions/games/Quatter/-/commits/master)

#### Compiling from source

If the binary is not working you may try compiling by running this line in a terminal:

```
git clone https://gitlab.com/luckeyproductions/games/Quatter.git; cd Quatter; ./install.sh; cd ..; rm -rf Quatter
```

Background image by [Greg Zaal](https://adaptivesamples.com)