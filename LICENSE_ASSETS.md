## Used licenses

_[CC0](https://creativecommons.org/publicdomain/zero/1.0/), [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/), [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)_

## Assets by type, author and license

### Various
##### Author irrelevant
- CC0
    + Docs/*
    + Screenshots/*
    + Resources/Materials/*
    + Resources/UI/DefaultStyle.xml 
    + Resources/Shaders/*
    + Resources/Techniques/*
    + Resources/RenderPaths/*
    + Resources/PostProcess/*
    + Resources/Textures/Ramp.png
    + Resources/Textures/Spot.png
    + Resources/Wood*.dds
    + Resources/Stone*.dds
    + Resources/Textures/UI/*.xml
    + Resources/Textures/*.xml
    
### Music
##### Angelight
- CC-BY 4.0
    + Resources/Music/Angelight - The Knowledge River.ogg [[Source]](https://www.jamendo.com/track/1239247/the-knowledge-river)
          
### 2D   
##### Modanung
- CC-BY-SA 4.0
    + Raw/*
    + quatter.svg
    + Resources/icon.png
    + Resources/Textures/Logo.png
    + Docs/Logo.png
    + Resources/UI/*.png
    + Resources/UI/*.dds
    + Resources/Textures/Board*.dds
    + Resources/Textures/LeafyMask.dds
    
- CC0
    + Resources/UI/UI.png
    + Resources/Textures/MossyForest*.png [[Source HDRI]]()

### 3D
##### Modanung
- CC-BY-SA 4.0
    + Blends/*
    + Resources/Models/\*.mdl
    + Resources/Models/\*/\*.mdl