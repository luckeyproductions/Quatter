#!/bin/sh

sudo chown -R $USER ~/.local/share/luckey/quatter/
sudo chown $USER ~/.local/share/icons/quatter.svg
update-icon-caches ~/.local/share/icons/
