/*
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef LUCKEY_H
#define LUCKEY_H

#include <Dry/Audio/Sound.h>
#include <Dry/Audio/SoundSource.h>
#include <Dry/Container/HashBase.h>
#include <Dry/Container/HashMap.h>
#include <Dry/Container/Vector.h>
#include <Dry/Core/CoreEvents.h>
#include <Dry/Engine/Application.h>
#include <Dry/Engine/Console.h>
#include <Dry/Engine/DebugHud.h>
#include <Dry/Engine/EngineDefs.h>
#include <Dry/Graphics/AnimatedModel.h>
#include <Dry/Graphics/AnimationController.h>
#include <Dry/Graphics/Animation.h>
#include <Dry/Graphics/AnimationState.h>
#include <Dry/Graphics/Camera.h>
#include <Dry/Graphics/DebugRenderer.h>
#include <Dry/Graphics/DecalSet.h>
#include <Dry/Graphics/Graphics.h>
#include <Dry/Graphics/Light.h>
#include <Dry/Graphics/Material.h>
#include <Dry/Graphics/Model.h>
#include <Dry/Graphics/Octree.h>
#include <Dry/Graphics/OctreeQuery.h>
#include <Dry/Graphics/ParticleEffect.h>
#include <Dry/Graphics/ParticleEmitter.h>
#include <Dry/Graphics/Renderer.h>
#include <Dry/Graphics/RenderPath.h>
#include <Dry/Graphics/Skybox.h>
#include <Dry/Graphics/StaticModel.h>
#include <Dry/Graphics/Technique.h>
#include <Dry/Graphics/Texture2D.h>
#include <Dry/Graphics/Viewport.h>
#include <Dry/Graphics/Zone.h>
#include <Dry/Input/InputEvents.h>
#include <Dry/Input/Input.h>
#include <Dry/IO/FileSystem.h>
#include <Dry/IO/Log.h>
#include <Dry/Math/MathDefs.h>
#include <Dry/Math/Plane.h>
#include <Dry/Math/Sphere.h>
#include <Dry/Math/Vector2.h>
#include <Dry/Math/Vector3.h>
#include <Dry/Physics/CollisionShape.h>
#include <Dry/Physics/PhysicsEvents.h>
#include <Dry/Physics/PhysicsWorld.h>
#include <Dry/Physics/RigidBody.h>
#include <Dry/Resource/ResourceCache.h>
#include <Dry/Resource/XMLFile.h>
#include <Dry/Scene/LogicComponent.h>
#include <Dry/Scene/Node.h>
#include <Dry/Scene/SceneEvents.h>
#include <Dry/Scene/Scene.h>
#include <Dry/UI/Button.h>
#include <Dry/UI/Font.h>
#include <Dry/UI/Text.h>
#include <Dry/UI/UI.h>
#include <Dry/UI/UIEvents.h>
#include <Dry/UI/Sprite.h>
#include <Dry/UI/UIComponent.h>
#include <Dry/UI/Window.h>
#include <Dry/Scene/ValueAnimation.h>

#include <Dry/DebugNew.h>

#define ENGINE GetSubsystem<Engine>()
#define CACHE GetSubsystem<ResourceCache>()
#define INPUT GetSubsystem<Input>()
#define GRAPHICS GetSubsystem<Graphics>()
#define RENDERER GetSubsystem<Renderer>()
#define TIME GetSubsystem<Time>()
#define FILES GetSubsystem<FileSystem>()
#define GUI GetSubsystem<UI>()

#define RES(x, y) GetSubsystem<ResourceCache>()->GetResource<x>(y)
#define RES_MAT(x) GetSubsystem<ResourceCache>()->GetResource<Material>(String{ "Materials/" } + x + ".xml")
#define RES_MDL(x) GetSubsystem<ResourceCache>()->GetResource<Model>(String{ "Models/" } + x + ".mdl")

#define FX GetSubsystem<EffectMaster>()

using namespace Dry;
namespace LucKey {

float Delta(float lhs, float rhs, bool angle = false);
Vector2 Rotate(const Vector2& vec2, const float angle);

}

#endif // LUCKEY_H
