/* Quatter
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "ui/guimaster.h"
#include "inputmaster.h"

#include "quattercam.h"

void QuatterCam::RegisterObject(Context *context)
{
    context->RegisterFactory<QuatterCam>();
}

QuatterCam::QuatterCam(Context* context): LogicComponent(context),
    zone_{ nullptr },
    distance_{ 16.f },
    aimDistance_{ distance_ },
    targetPosition_{ TARGET_BOARD * 23.f },
    aimTargetPosition_{ targetPosition_ },
    toPitch_{}
{
    SetUpdateEventMask(USE_UPDATE);
    SubscribeToEvent(E_GRAPHICSQUALITYCHANGED, DRY_HANDLER(QuatterCam, HandleGraphicsQualityChanged));
}

void QuatterCam::Start()
{
    node_->SetPosition(Vector3{ 3.f, 8.f, -10.f }.Normalized() * distance_);
    node_->LookAt(targetPosition_);
    camera_ = node_->CreateComponent<Camera>();
    camera_->SetFarClip(1024.f);
    UpdateFov();

    zone_ = node_->CreateComponent<Zone>();
    zone_->SetFogColor(Color{ .16f, .17f, .165f }); // Leafy knoll: Color{ .20f, .17f, .13f }
    zone_->SetFogStart(50.f);
    zone_->SetFogEnd(64.f);
//    zone->SetZoneTexture(GetScene()->GetChild("Sky")->GetComponent<Skybox>()->GetMaterial(0)->GetTexture(TU_DIFFUSE));

    CreatePockets();
    SetupViewport();

    UpdateGraphics();
}

void QuatterCam::CreatePockets()
{
    for (bool left: { true, false })
    {
        Node* pocketNode{ node_->CreateChild("Pocket", LOCAL) };
        pocketNode->SetRotation(Quaternion(-70.f, Vector3::RIGHT));

        if (left)
            pockets_.first_ = pocketNode;
        else
            pockets_.second_ = pocketNode;
    }
}

void QuatterCam::SetupViewport()
{
    viewport_ = new Viewport(context_, GetScene(), camera_);

    //Add anti-asliasing and bloom
    effectRenderPath_ = viewport_->GetRenderPath();
    effectRenderPath_->Append(CACHE->GetResource<XMLFile>("PostProcess/FXAA3.xml"));
    effectRenderPath_->Append(CACHE->GetResource<XMLFile>("PostProcess/BloomHDR.xml"));
    effectRenderPath_->SetShaderParameter("BloomHDRThreshold", .34f);
    effectRenderPath_->SetShaderParameter("BloomHDRMix", Vector2(1.023f, .17f));

    RENDERER->SetViewport(0, viewport_);
}

void QuatterCam::UpdateGraphics()
{
    const GraphicsQuality quality{ MC->GetGraphicsQuality() };

    effectRenderPath_->SetEnabled("FXAA3", quality != QUALITY_LOW);
    effectRenderPath_->SetEnabled("BloomHDR", quality == QUALITY_HIGH);
    zone_->SetAmbientColor(Color{ .22f, .23f, .225f }.Lerp( Color{ .33f, .333f, .332f }, quality == QUALITY_LOW));
}

void QuatterCam::UpdateFov()
{
    camera_->SetFov(Clamp(60.f + distance_, 23.f, 110.f));
}

void QuatterCam::Update(float timeStep)
{
    if (toPitch_ > 0.f)
    {
        const float pitch{ Min(toPitch_, toPitch_ * Min(1.f, timeStep * 7.f)) };
        toPitch_ -= pitch;
        Rotate({ 0.f, pitch });
    }

    const Vector3 oldPos{ node_->GetPosition() };

//    if (effectRenderPath_->GetShaderParameter("BloomHDRMix").GetVector2().y_ != .7f)
//        effectRenderPath_->SetShaderParameter("BloomHDRMix", Vector2(Clamp(TIME->GetElapsedTime() * .5f - 1.f, 0.f, 1.f), Max(0.7f, 2.3f - TIME->GetElapsedTime() * .25f)));

    if (MC->InMenu())
    {
        if (aimTargetPosition_.y_ > 1.f)
        {
            node_->RotateAround(targetPosition_, Quaternion(timeStep, Vector3::UP), TS_WORLD);
        }
        else
        {
            if (node_->GetWorldPosition().y_ - aimTargetPosition_.y_ > 0.f)
                node_->Translate(Vector3::DOWN * (node_->GetWorldPosition().y_ - aimTargetPosition_.y_) * timeStep * 4.2f);

            int side{ GetSubsystem<GUIMaster>()->GetActiveSide() };//Floor(TIME->GetElapsedTime() * .5f) };
            Vector3 tableSideCrossProduct{ node_->GetDirection().CrossProduct(Quaternion(-90.f * side, Vector3::UP) * MC->world_.tableNode_->GetDirection()) };

            if (tableSideCrossProduct.Length())
            {
                node_->RotateAround(targetPosition_, Quaternion(tableSideCrossProduct.Length() * 235.f * timeStep, tableSideCrossProduct), TS_WORLD);
                node_->LookAt(targetPosition_, .5f * (node_->GetUp() + Vector3::UP));
            }

            aimDistance_ = ZOOM_MENU * (1.f + .23f * tableSideCrossProduct.Length());
        }
    }

    //Update distance
    if (distance_ != aimDistance_)
        distance_ = .1f * (9.f * distance_ + aimDistance_);

    //Update target position
    if (targetPosition_ != aimTargetPosition_)
        targetPosition_ = .2f * (4.f * targetPosition_ + aimTargetPosition_);

    Vector3 relativeToTarget{ (node_->GetPosition() - targetPosition_).Normalized() };

    if (relativeToTarget.Length() != distance_)
    {
        node_->SetPosition(distance_ * relativeToTarget + targetPosition_);
        UpdateFov();
    }

    //Spin pockets
    UpdatePockets(timeStep);

    if (oldPos != node_->GetPosition())
        GetSubsystem<InputMaster>()->UpdateYad();
}

void QuatterCam::UpdatePockets(float timeStep)
{
    float spinSpeed{ 23.f };

    pockets_.first_->Rotate(Quaternion(timeStep * spinSpeed, Vector3::UP));
    pockets_.second_->Rotate(Quaternion(-timeStep * spinSpeed, Vector3::UP));
    //Reposition pockets
    pockets_.first_->SetPosition(Vector3(-2.f - .05f * (ZOOM_MAX - distance_), 1.f - GetPitch() * .01f, 3.f));
    pockets_.second_->SetPosition(Vector3(2.f + .05f * (ZOOM_MAX - distance_), 1.f - GetPitch() * .01f, 3.f));
}

void QuatterCam::Rotate(const Vector2& rotation)
{
    if (MC->InMenu())
        return;

    node_->LookAt(targetPosition_);
    node_->RotateAround(targetPosition_,
                        Quaternion(rotation.x_, Vector3::UP) * Quaternion(rotation.y_, node_->GetRight()),
                        TS_WORLD);

    const Vector3 camRight{ node_->GetRight() };

    //Clamp pitch
    if (GetPitch() > PITCH_MAX)
        node_->RotateAround(targetPosition_,
                                Quaternion(PITCH_MAX - GetPitch(), camRight),
                                TS_WORLD);
    else if (GetPitch() < PITCH_MIN)
        node_->RotateAround(targetPosition_,
                                Quaternion(PITCH_MIN - GetPitch(), camRight),
                                TS_WORLD);
}

void QuatterCam::Zoom(float delta)
{
    if (MC->InMenu())
        return;

    aimDistance_ = Clamp(aimDistance_ - delta, ZOOM_MIN, ZOOM_MAX);
}

void QuatterCam::TargetMenu()
{
    aimTargetPosition_ = TARGET_MENU;
}

void QuatterCam::TargetBoard()
{
    aimTargetPosition_ = TARGET_BOARD;
    aimDistance_ = Lerp(ZOOM_MIN, ZOOM_MAX, 1/3.f);
    toPitch_ = 60.f;
}
