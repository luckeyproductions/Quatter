/*
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "luckey.h"

float LucKey::Delta(float lhs, float rhs, bool angle)
{
    if (!angle)
    {
        return (lhs > rhs) ? Abs(lhs - rhs) : Abs(rhs - lhs);
    }
    else
    {
        lhs = Cycle(lhs, 0.0f, 360.0f);
        rhs = Cycle(rhs, 0.0f, 360.0f);
        if (Delta(lhs, rhs) > 180.0f)
            return Abs(360.0f - Delta(lhs, rhs));
        else
            return Delta(lhs, rhs);
    }
}

Vector2 LucKey::Rotate(const Vector2& vec2, const float angle)
{
    const float theta{ M_DEGTORAD * angle };
    const float cs{ cos(theta) };
    const float sn{ sin(theta) };
    const float x{ vec2.x_ };
    const float y{ vec2.y_ };

    return { x * cs - y * sn, x * sn + y * cs };
}
