/* Quatter
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef AI_H
#define AI_H

#include "mastercontrol.h"
#include "board.h"

enum Personality{ AP_DICEY = 0, AP_SAFE, AP_WIN, AP_SAFEWIN, AP_ALL };

class AI: public Object
{
    DRY_OBJECT(AI, Object);

public:
    AI(Context* context);

    void Reset(bool demo = false);
    void TakeTurn(float timeStep);
    void UpdateComposition() { composition_ = BOARD->GetComposition(); }

private:
    static Personality RandomPersonality() { return static_cast<Personality>(Random(AP_ALL)); }
    Personality GetCurrentPersonality();
    void DiceyTurn();
    void SafeTurn();
    void WinTurn();
    void SafeWinTurn();

    Vector<IntVector2> FindWinningMoves(int piece = -1);
    Vector<int> FindSafePieces();


    Pair<Personality, Personality> personality_;
    float delay_;
    Composition composition_;
};

#endif // AI_H
