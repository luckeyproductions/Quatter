/* Quatter
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef MASTERCONTROL_H
#define MASTERCONTROL_H

#include "luckey.h"

namespace Dry {
class Node;
class Scene;
}

using namespace Dry;

class QuatterCam;
class InputMaster;
class EffectMaster;
class Board;
class Piece;

enum class GameState{ SPLASH = 0, MENU, PLAYER1PICKS, PLAYER2PUTS, PLAYER2PICKS, PLAYER1PUTS, QUATTER };
enum GameMode{ GM_NONE = 0, GM_PVP_LOCAL, GM_PVP_NETWORK, GM_PVC, GM_CVC};
enum MusicState{ MUSIC_OFF = 0, MUSIC_SONG1, MUSIC_SONG2 };
enum SelectionMode{ SM_CAMERA = 0, SM_STEP, SM_YAD };

typedef struct GameWorld
{
    QuatterCam* camera_;
    Scene* scene_;
    Board* board_;
    Node* tableNode_;
    Vector<Piece*> pieces_;
} GameWorld;

#define MC GetSubsystem<MasterControl>()
#define SETTINGSPATH GetStoragePath() + "/Settings.xml"

#define NUM_PIECES 16
#define TABLE_DEPTH .21f
#define RESET_DURATION 1.23f
#define IDLE_THRESHOLD 5.f

#define COLOR_GLOW MC->GetMaterial("Glow")->GetShaderParameter("MatDiffColor").GetColor()

DRY_EVENT(E_GRAPHICSQUALITYCHANGED, GraphicsQualityChanged) {}

using GraphicsQuality = MaterialQuality;

class MasterControl: public Application
{
    DRY_OBJECT(MasterControl, Application);
    friend class InputMaster;

public:
    MasterControl(Context* context);
    String GetStoragePath() const { return storagePath_; }

    GameWorld world_;

    void Setup() override;
    void Start() override;
    void Stop() override;

    void Exit();
    void CreateLights();

    GameMode GetGameMode() const noexcept { return gameMode_; }
    GameState GetGameState() const noexcept { return gameState_; }
    GameState GetPreviousGameState() const noexcept { return previousGameState_; }
    bool GameOver() const;
    bool InMenu() const noexcept { return gameState_ == GameState::MENU || gameState_ == GameState::SPLASH; }
    bool InPickState() const noexcept { return gameState_ == GameState::PLAYER1PICKS || gameState_ == GameState::PLAYER2PICKS; }
    bool InPutState() const noexcept { return gameState_ == GameState::PLAYER1PUTS || gameState_ == GameState::PLAYER2PUTS; }
    bool InPlayer1State() const noexcept { return gameState_ == GameState::PLAYER1PICKS || gameState_ == GameState::PLAYER1PUTS; }
    bool InPlayer2State() const noexcept { return gameState_ == GameState::PLAYER2PICKS || gameState_ == GameState::PLAYER2PUTS; }

    void NextPhase();
    void NextSelectionMode();
    void SetSelectionMode(SelectionMode mode);
    void SetEnableAI(bool enabled) { gameMode_ = (enabled ? GM_PVC : GM_PVP_LOCAL); }
    void StartDemo();
    bool IsAITurn() const { return gameState_ != GameState::MENU && (gameMode_ == GM_CVC || (gameMode_ == GM_PVC && InPlayer2State())); }
    void NextMusicState();
    void TakeScreenshot();

    float AttributesToAngle(int attributes) const { return (360.0f / NUM_PIECES * attributes) + 180.0f / NUM_PIECES + 23.5f; }
    Vector3 AttributesToPosition(int attributes) const {
        return Quaternion(AttributesToAngle(attributes), Vector3::UP) * Vector3::BACK * 7.0f
                + Vector3::DOWN * TABLE_DEPTH;
    }

    Material* GetMaterial(const String& name) const { return CACHE->GetResource<Material>("Materials/" + name + ".xml"); }
    Model* GetModel(const String& name) const { return CACHE->GetResource<Model>("Models/" + name + ".mdl"); }
    Sound* GetMusic(const String& name) const;
    Sound* GetSample(const String& name) const;

    void Quatter();
    void SetPickedPiece(Piece* piece) { pickedPiece_ = piece; }
    void StepSelectPiece(bool next);
    Piece* GetSelectedPiece() const { return selectedPiece_; }
    Piece* GetPickedPiece() const { return pickedPiece_; }
    void DeselectPiece();
    Vector<Piece*> GetFreePieces() const;

    float Sine(float freq, float min = -1.f, float max = 1.f, float shift = 0.f);
    float Cosine(float freq, float min = -1.f, float max = 1.f, float shift = 0.f);

    void EnterMenu();
    void Reset();

    void SetGraphicsQuality(GraphicsQuality quality);
    GraphicsQuality GetGraphicsQuality() const { return graphicsQuality_; }

private:
    GraphicsQuality graphicsQuality_;
    String storagePath_;

    Node* leafyLightNode_;
    Light* downwardsLight_;
    Light* leafyLight_;
    Vector<Light*> pointLights_;
    StaticModel* tableModel_;

    SoundSource* musicSource1_;
    SoundSource* musicSource2_;
    float musicGain_;

    GameMode gameMode_;
    GameState gameState_;
    GameState previousGameState_;
    GameState startGameState_;
    MusicState musicState_;
    MusicState previousMusicState_;
    SelectionMode selectionMode_;

    Piece* selectedPiece_;
    Piece* lastSelectedPiece_;
    Piece* pickedPiece_;

    float lastReset_;

    void CreateScene();
    void HandleUpdate(StringHash eventType, VariantMap& eventData);

    void CameraSelectPiece(bool force = false);
    void SelectPrevious();

    void MusicGainUp(float step);
    void MusicGainDown(float step);

    void SelectPiece(Piece* piece);
    bool SelectLastPiece();

    bool IsLame() { return TIME->GetElapsedTime() - lastReset_ < (RESET_DURATION + .23f); }
    void CreateJukebox();
    void CreateSkybox();
    void CreateTable();
    void CreateBoardAndPieces();
    void LoadSettings();
    void SaveSettings();
    void UpdateGraphics();
    void SetResourcePaths();
};

#endif // MASTERCONTROL_H
