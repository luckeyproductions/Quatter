/* Quatter
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "mastercontrol.h"
#include "inputmaster.h"
#include "effectmaster.h"
#include "ai.h"
#include "quattercam.h"
#include "piece.h"
#include "board.h"
#include "square.h"
#include "ui/guimaster.h"
#include "ui/indicator.h"
#include "ui/yad.h"

DRY_DEFINE_APPLICATION_MAIN(MasterControl);

MasterControl::MasterControl(Context* context): Application(context),
    graphicsQuality_{ QUALITY_MAX },
    storagePath_{},
    leafyLightNode_{ nullptr },
    downwardsLight_{ nullptr },
    leafyLight_{ nullptr },
    pointLights_{},
    tableModel_{ nullptr },
    musicSource1_{ nullptr },
    musicSource2_{ nullptr },
    musicGain_{ 1.f },
    gameMode_{ GM_NONE },
    gameState_{ GameState::SPLASH },
    previousGameState_{},
    startGameState_{ gameState_ },
    musicState_{ MUSIC_SONG1 },
    previousMusicState_{ MUSIC_OFF },
    selectionMode_{ SM_CAMERA },
    selectedPiece_{},
    lastSelectedPiece_{},
    pickedPiece_{},
    lastReset_{ 0.f }
{
}

void MasterControl::Setup()
{
    SetRandomSeed(TIME->GetSystemTime());

    engineParameters_[EP_LOG_NAME] = FILES->GetAppPreferencesDir("luckey", "quatter") + "quatter.log";
    engineParameters_[EP_WINDOW_TITLE] = "Quatter";
    engineParameters_[EP_WINDOW_ICON] = "icon.png";
    engineParameters_[EP_FULL_SCREEN] = true;

    SetResourcePaths();
    LoadSettings();

    if (graphicsQuality_ == QUALITY_MAX)
        SetGraphicsQuality(QUALITY_HIGH);
}

void MasterControl::SetResourcePaths()
{
    FileSystem* fs{ GetSubsystem<FileSystem>() };
    String resourcePath{ "Resources" };
    if (!fs->DirExists(AddTrailingSlash(fs->GetProgramDir()) + resourcePath))
    {
        const String installedResources{ RemoveTrailingSlash(RESOURCEPATH) };
        if (fs->DirExists(installedResources))
            resourcePath = installedResources;
    }

    if (resourcePath == "Resources")
        storagePath_ = resourcePath;
    else
        storagePath_ = RemoveTrailingSlash(fs->GetAppPreferencesDir("luckey", "tux"));

    engineParameters_[EP_RESOURCE_PATHS] = resourcePath;
}

void MasterControl::Start()
{
    QuatterCam::RegisterObject(context_);
    Piece::RegisterObject(context_);
    Board::RegisterObject(context_);
    Square::RegisterObject(context_);
    Indicator::RegisterObject(context_);
    Yad::RegisterObject(context_);

    context_->RegisterSubsystem(this);
    context_->RegisterSubsystem<InputMaster>();
    context_->RegisterSubsystem<EffectMaster>();
    context_->RegisterSubsystem<AI>();

    CreateScene();

    context_->RegisterSubsystem<GUIMaster>();

    SubscribeToEvent(E_UPDATE, DRY_HANDLER(MasterControl, HandleUpdate));
    INPUT->SetMousePosition(GRAPHICS->GetSize() / 2);
}

void MasterControl::Stop()
{
    SaveSettings();
//    engine_->DumpResources(true);
}

void MasterControl::Exit()
{
//    File file(context_, "Resources/Endgame.xml", FILE_WRITE);
//    world_.scene_->SaveXML(file);

    engine_->Exit();
}

void MasterControl::LoadSettings()
{
    const String settingsLocation{ SETTINGSPATH };

    if (FILES->FileExists(settingsLocation))
    {
        File file(context_, settingsLocation, FILE_READ);
        XMLFile configFile(context_);
        configFile.Load(file);
        const XMLElement graphics{ configFile.GetRoot().GetChild("Graphics") };
        const XMLElement audio{ configFile.GetRoot().GetChild("Audio") };

        if (graphics)
        {
            engineParameters_[EP_WINDOW_WIDTH] = graphics.GetInt("Width");
            engineParameters_[EP_WINDOW_HEIGHT] = graphics.GetInt("Height");
            engineParameters_[EP_FULL_SCREEN] = graphics.GetBool("Fullscreen");
            SetGraphicsQuality(static_cast<GraphicsQuality>(Clamp(graphics.GetUInt("Quality"), 0u, 2u)));
        }

        if (audio)
            musicGain_ = audio.GetFloat("MusicGain");
    }
}

void MasterControl::SaveSettings()
{
    XMLFile file(context_);
    XMLElement root(file.CreateRoot("Settings"));

    XMLElement graphicsElement(root.CreateChild("Graphics"));
    graphicsElement.SetInt("Width", GRAPHICS->GetWidth());
    graphicsElement.SetInt("Height", GRAPHICS->GetHeight());
    graphicsElement.SetBool("Fullscreen", GRAPHICS->GetFullscreen());
    graphicsElement.SetUInt("Quality", graphicsQuality_);

    XMLElement audioElement(root.CreateChild("Audio"));
    audioElement.SetFloat("MusicGain", musicGain_);

    file.SaveFile(storagePath_ + "/Settings.xml");
}

void MasterControl::CreateScene()
{
    world_.scene_ = new Scene(context_);
    world_.scene_->CreateComponent<Octree>();

    CreateSkybox();

    Node* cameraNode{ world_.scene_->CreateChild("Camera", LOCAL) };
    world_.camera_ = cameraNode->CreateComponent<QuatterCam>();

    CreateLights();
    CreateJukebox();
    CreateTable();
    CreateBoardAndPieces();

    GetSubsystem<InputMaster>()->ConstructYad();

    UpdateGraphics();
}

void MasterControl::CreateLights()
{
    //Add a directional light to the world. Enable cascaded shadows on it
    Node* downardsLightNode{ world_.scene_->CreateChild("DirectionalLight", LOCAL) };
    downardsLightNode->SetPosition(Vector3{ 2.f, 23.f, 3.f });
    downardsLightNode->LookAt(Vector3::ZERO);

    downwardsLight_ = downardsLightNode->CreateComponent<Light>();
    downwardsLight_->SetLightType(LIGHT_DIRECTIONAL);
    downwardsLight_->SetColor(Color{ .8f, .9f, .95f });
    downwardsLight_->SetCastShadows(true);
    downwardsLight_->SetShadowBias(BiasParameters{ .000025f, .5f });
    downwardsLight_->SetShadowCascade(CascadeParameters{ 7.f, 13.f, 23.f, 42.f, .6f });

    //Add leafy light source
    leafyLightNode_ = world_.scene_->CreateChild("LeafyLight", LOCAL);
    leafyLightNode_->SetPosition(Vector3{ 6.f, 96.f, 9.0f });
    leafyLightNode_->LookAt(Vector3{ 0.f, 0.f, 0.f });
    leafyLight_ = leafyLightNode_->CreateComponent<Light>();
    leafyLight_->SetLightType(LIGHT_SPOT);
    leafyLight_->SetRange(180.f);
    leafyLight_->SetFov(34.f);
    leafyLight_->SetShapeTexture(static_cast<Texture*>(CACHE->GetResource<Texture2D>("Textures/LeafyMask.dds")));

    //Create point lights
    for (const Vector3& pos: { Vector3{ -10.f,  8.f, -23.f },
                               Vector3{ -20.f, -8.f,  23.f },
                               Vector3{  20.f, -7.f,  23.f }}) {

        Node* pointLightNode_{ world_.scene_->CreateChild("PointLight", LOCAL) };
        pointLightNode_->SetPosition(pos);

        Light* pointLight{ pointLightNode_->CreateComponent<Light>() };
        pointLight->SetLightType(LIGHT_POINT);
        pointLight->SetBrightness(.42f);
        pointLight->SetRange(42.0f);
        pointLight->SetColor(Color{ .75f, 1.0f, .75f });
        pointLight->SetShadowResolution(.25f);
        pointLight->SetShadowIntensity(.6f);
        pointLights_.Push(pointLight);
    }
}

bool MasterControl::GameOver() const
{
    return MC->GetGameState() == GameState::QUATTER || BOARD->IsFull();
}

void MasterControl::CreateSkybox()
{
    Node* skyNode{ world_.scene_->CreateChild("Sky", LOCAL) };
    Skybox* skybox{ skyNode->CreateComponent<Skybox>() };
    skybox->SetModel(GetModel("Box"));
    skybox->SetMaterial(GetMaterial("MossyForest"));
}

void MasterControl::CreateJukebox()
{
    Sound* song1{ GetMusic("Angelight - The Knowledge River") };
    Sound* song2{ GetMusic("Angelight - The Knowledge River") };
    Node* musicNode{ world_.scene_->CreateChild("Music", LOCAL) };

    musicSource1_ = musicNode->CreateComponent<SoundSource>();
    musicSource1_->SetSoundType(SOUND_MUSIC);
    musicSource1_->SetGain(musicGain_);
    musicSource1_->Play(song1);

    musicSource2_ = musicNode->CreateComponent<SoundSource>();
    musicSource2_->SetSoundType(SOUND_MUSIC);
    musicSource2_->SetGain(.0f);
    musicSource2_->Play(song2);
}

void MasterControl::CreateTable()
{
    world_.tableNode_ = world_.scene_->CreateChild("Table", LOCAL);
    world_.tableNode_->AddTag("Table");
    world_.tableNode_->SetRotation(Quaternion{ 23.5f, Vector3::UP });
    tableModel_ = world_.tableNode_->CreateComponent<StaticModel>();

    Node* hitNode{ world_.scene_->CreateChild("HitPlane", LOCAL) };
    hitNode->SetPosition(Vector3::DOWN * 1.23f);
    hitNode->SetScale(128.0f);

    StaticModel* hitPlane{ hitNode->CreateComponent<StaticModel>() };
    hitPlane->SetModel(MC->GetModel("Plane"));
    hitPlane->SetMaterial(MC->GetMaterial("Invisible"));
}

void MasterControl::CreateBoardAndPieces()
{
    Node* boardNode{ world_.scene_->CreateChild("Board", LOCAL) };
    world_.board_ = boardNode->CreateComponent<Board>();
    world_.pieces_.Reserve(NUM_PIECES);

    for (int p{ 0 }; p < NUM_PIECES; ++p)
    {
        Node* pieceNode{ world_.scene_->CreateChild("Piece", LOCAL) };
        Piece* newPiece{ pieceNode->CreateComponent<Piece>() };
        newPiece->Init(Piece::PieceAttributes(p));

        world_.pieces_.Push(newPiece);
        newPiece->SetPosition(AttributesToPosition(newPiece->ToInt())
                              + Vector3{ Random(.05f), .0f, Random(.05f) });
    }
}

Sound* MasterControl::GetMusic(const String& name) const
{
    Sound* song{ CACHE->GetResource<Sound>("Music/" + name + ".ogg") };
    song->SetLooped(true);

    return song;
}

Sound* MasterControl::GetSample(const String& name) const
{
    Sound* sample{ CACHE->GetResource<Sound>("Samples/" + name + ".ogg") };
    sample->SetLooped(false);

    return sample;
}

void MasterControl::HandleUpdate(StringHash /*eventType*/, VariantMap& eventData)
{
    if (selectionMode_ == SM_CAMERA && !GetSubsystem<InputMaster>()->IsIdle())
        CameraSelectPiece();

    //Wave leafy light
    leafyLightNode_->SetRotation(Quaternion(90.f + PowN(MC->Sine(.05f, -1.f, 1.f), 3) * .23f, Vector3::RIGHT) *
                                 Quaternion(MC->Sine(.17f, 178.f, 182.f), Vector3::FORWARD));
    leafyLight_->SetBrightness(.34f + MC->Sine(.0042f, .05f, .23f) + MC->Sine(.017f, .05f, .13f));

    if (IsAITurn())
        GetSubsystem<AI>()->TakeTurn(eventData[Update::P_TIMESTEP].GetFloat());
}

void MasterControl::SelectPiece(Piece* piece)
{
    DeselectPiece();
    selectedPiece_ = piece;
    piece->Select();
}

void MasterControl::CameraSelectPiece(bool force)
{
    if ((!force && IsLame()) || IsAITurn())
        return;

    Piece* nearest{ selectedPiece_ };

    for (Piece* piece: world_.pieces_)
    {
        if (!nearest
         && (piece->GetState() == PieceState::FREE || piece->GetState() == PieceState::SELECTED))
        {
            nearest = piece;
        }
        else if ((piece->GetState() == PieceState::FREE || piece->GetState() == PieceState::SELECTED) &&
                   CAMERA->GetPosition().DistanceToPoint(piece->GetPosition())    <
                   CAMERA->GetPosition().DistanceToPoint(nearest->GetPosition()))
        {
            nearest = piece;
        }
    }

    if (nearest != selectedPiece_)
        SelectPiece(nearest);
}

bool MasterControl::SelectLastPiece()
{
    if (lastSelectedPiece_)
    {
        SelectPiece(lastSelectedPiece_);
        return true;
    }
    else
    {
        return false;
    }
}

void MasterControl::StepSelectPiece(bool next)
{
    SetSelectionMode(SM_STEP);

    if (selectedPiece_)
    {
        int selectInt{ selectedPiece_->ToInt() };

        while (world_.pieces_.At(selectInt)->GetState() != PieceState::FREE)
        {
            if (next)
            {
                selectInt -= 1;
                if (selectInt < 0)
                    selectInt = NUM_PIECES - 1;
            }
            else
            {
                selectInt += 1;
                if (selectInt > NUM_PIECES - 1)
                    selectInt = 0;
            }
        }

        SelectPiece(world_.pieces_.At(selectInt));
    }
    else if (!SelectLastPiece())
    {
        if (IsAITurn())
        {
            const Vector<Piece*> pieces{ GetFreePieces() };
            SelectPiece(pieces.At(Random(static_cast<int>(pieces.Size()))));
        }
        else
        {
            CameraSelectPiece(true);
        }
    }
}

void MasterControl::DeselectPiece()
{
    if (selectedPiece_)
    {
        lastSelectedPiece_ = selectedPiece_;
        selectedPiece_->Deselect();
    }

    selectedPiece_ = nullptr;
}

Vector<Piece*> MasterControl::GetFreePieces() const
{
    Vector<Piece*> pieces{ world_.pieces_ };
    for (Piece* p: pieces)
        if (p->GetState() != PieceState::FREE)
            pieces.Remove(p);
    return pieces;
}

void MasterControl::NextPhase()
{    
    if (gameState_ == GameState::QUATTER)
        return;

    DeselectPiece();

    previousGameState_ = gameState_;
    GUIMaster* gm{ GetSubsystem<GUIMaster>() };

    switch (gameState_)    {
    case GameState::PLAYER1PICKS: {
        gameState_ = GameState::PLAYER2PUTS;
        gm->ShowPlayerBanner(2);
        BOARD->SelectNearestFreeSquare(CAMERA->GetPosition());
    } break;
    case GameState::PLAYER2PUTS: {
        gameState_ = GameState::PLAYER2PICKS;
        if (selectionMode_ != SM_YAD)
            CameraSelectPiece();
    } break;
    case GameState::PLAYER2PICKS: {
        gameState_ = GameState::PLAYER1PUTS;
        gm->ShowPlayerBanner(1);
        BOARD->SelectNearestFreeSquare(CAMERA->GetPosition());
    } break;
    default: case GameState::PLAYER1PUTS: {
        gameState_ = GameState::PLAYER1PICKS;
        if (selectionMode_ != SM_YAD)
            CameraSelectPiece();
    } break;
    }

    if (IsAITurn() && InPickState())
        GetSubsystem<AI>()->UpdateComposition();
}

void MasterControl::Quatter()
{
    previousGameState_ = gameState_;
    gameState_ = GameState::QUATTER;

    world_.camera_->ZoomToBoard();
}

void MasterControl::Reset()
{
    lastReset_ = TIME->GetElapsedTime();

    for (Piece* p: world_.pieces_)
        p->Reset();

    world_.board_->Reset();
    GetSubsystem<AI>()->Reset(gameMode_ == GM_CVC);

    GUIMaster* gm{ GetSubsystem<GUIMaster>() };
    lastSelectedPiece_ = nullptr;

    if (gameState_ == GameState::MENU)
    {
        GetSubsystem<GUIMaster>()->SetLogoVisible(false);
        gameState_ = GameState::PLAYER1PICKS;
        gm->ShowPlayerBanner(1);
    }
    else if (gameState_ == GameState::QUATTER)
    {
        if (previousGameState_ == GameState::PLAYER1PUTS)
        {
            gameState_ = GameState::PLAYER1PICKS;
            gm->ShowPlayerBanner(1);
        }
        else if (previousGameState_ == GameState::PLAYER2PUTS)
        {
            gameState_ = GameState::PLAYER2PICKS;
            gm->ShowPlayerBanner(2);
        }

        world_.camera_->ZoomToTable();
    }
    else
    {
        if (startGameState_ == GameState::PLAYER1PICKS)
        {
            gameState_ = GameState::PLAYER2PICKS;
            gm->ShowPlayerBanner(2);
        }
        else if (startGameState_ == GameState::PLAYER2PICKS)
        {
            gameState_ = GameState::PLAYER1PICKS;
            gm->ShowPlayerBanner(1);
        }
    }

    startGameState_ = gameState_;
}

void MasterControl::SetGraphicsQuality(GraphicsQuality quality)
{
    if (graphicsQuality_ == quality)
        return;

    graphicsQuality_ = quality;

    if (RENDERER)
    {
        SendEvent(E_GRAPHICSQUALITYCHANGED);

        RENDERER->SetMaterialQuality(graphicsQuality_);
        RENDERER->SetTextureQuality(graphicsQuality_);

        UpdateGraphics();
    }
    else
    {
        engineParameters_[EP_MATERIAL_QUALITY] = graphicsQuality_;
        engineParameters_[EP_TEXTURE_QUALITY] = graphicsQuality_;
    }
}

void MasterControl::UpdateGraphics()
{
    if (tableModel_)
    {
        const String postfix{ GetGraphicsQuality() == QUALITY_LOW ? "_LOQ" : "" };
        tableModel_->SetModel(GetModel(String{ "Table" } + postfix));
        tableModel_->SetMaterial(GetMaterial("Table"));
        tableModel_->SetCastShadows(GetGraphicsQuality() != QUALITY_LOW);
    }

    if (downwardsLight_)
    {
        const int brightnessFactor{ 2 - static_cast<int>(graphicsQuality_) };
        downwardsLight_->SetBrightness(.55f + .1f * brightnessFactor);
        downwardsLight_->SetSpecularIntensity(1.f - .17f * brightnessFactor);
        downwardsLight_->SetShadowResolution(.25f * PowN(2, graphicsQuality_));
        downwardsLight_->SetShadowIntensity(.1f + .2f * brightnessFactor);
    }

    if (leafyLight_)
        leafyLight_->SetEnabled(graphicsQuality_ != QUALITY_LOW);

    for (Light* light: pointLights_)
    {
        light->SetEnabled(graphicsQuality_ != QUALITY_LOW);
        light->SetCastShadows(graphicsQuality_ == QUALITY_HIGH);
    }
}

void MasterControl::NextSelectionMode()
{
    switch (selectionMode_)
    {
    case SM_CAMERA:
        SetSelectionMode(SM_YAD);
        break;
    case SM_STEP:
        SetSelectionMode(SM_CAMERA);
        break;
    case SM_YAD:
        SetSelectionMode(SM_CAMERA);
        break;
    default: break;
    }
}

void MasterControl::SetSelectionMode(SelectionMode mode)
{
    if (selectionMode_ == mode)
        return;

    selectionMode_ = mode;

    if (IsAITurn())
        return;

    switch (mode)
    {
    default: case SM_CAMERA:
        CameraSelectPiece();
    break;
    case SM_STEP:
    break;
    case SM_YAD:
        DeselectPiece();
    break;
    }
}

void MasterControl::StartDemo()
{
    gameMode_ = GM_CVC;
    MC->world_.camera_->TargetBoard();
    Reset();
}

void MasterControl::NextMusicState()
{
    //Fade out
    if (musicState_ == MUSIC_SONG1)
    {
        previousMusicState_ = musicState_;
        musicState_ = MUSIC_OFF;
        FX->FadeOut(musicSource1_);
    }
    else if (musicState_ == MUSIC_SONG2)
    {
        previousMusicState_ = musicState_;
        musicState_ = MUSIC_OFF;
        FX->FadeOut(musicSource2_);
    //Pick song
    }
    else if (musicState_ == MUSIC_OFF)
    {
        if (previousMusicState_ == MUSIC_SONG1)
            musicState_ = MUSIC_SONG2;
        else if (previousMusicState_ == MUSIC_SONG2)
            musicState_ = MUSIC_SONG1;
        previousMusicState_ = musicState_;
    }

    //Fade in
    if (musicState_ == MUSIC_SONG1)
        FX->FadeTo(musicSource1_, Max(musicGain_, 0.1f));
    else if (musicState_ == MUSIC_SONG2)
        FX->FadeTo(musicSource2_, Max(musicGain_, 0.1f));
}

void MasterControl::MusicGainUp(float step)
{
    musicGain_ = Clamp(musicGain_ + step, step, 1.0f);

    if (musicState_ == MUSIC_SONG1)
        FX->FadeTo(musicSource1_, musicGain_, 0.23f);
    else if (musicState_ == MUSIC_SONG2)
        FX->FadeTo(musicSource2_, musicGain_, 0.23f);
}

void MasterControl::MusicGainDown(float step)
{
    musicGain_ = Clamp(musicGain_ - step, 0.0f, 1.0f);

    if (musicState_ == MUSIC_SONG1)
        FX->FadeTo(musicSource1_, musicGain_, 0.23f);
    else if (musicState_ == MUSIC_SONG2)
        FX->FadeTo(musicSource2_, musicGain_, 0.23f);
}

void MasterControl::EnterMenu()
{
    gameState_ = GameState::MENU;
    gameMode_ = GM_NONE;
    world_.camera_->TargetMenu();

    GUIMaster* gm{ GetSubsystem<GUIMaster>() };
    gm->SetLogoVisible(true);
    gm->ShowPlayerBanner(0u);

    GetSubsystem<InputMaster>()->ResetIdle();
}

void MasterControl::TakeScreenshot()
{
    Image screenshot{ context_ };
    GRAPHICS->TakeScreenShot(screenshot);
    //Here we save in the Screenshots folder with date and time appended
    String fileName{ GetSubsystem<FileSystem>()->GetProgramDir() + "Screenshots/Screenshot_" +
                Time::GetTimeStamp().Replaced(':', '_').Replaced('.', '_').Replaced(' ', '_') + ".png" };
    Log::Write(1, fileName);
    screenshot.SavePNG(fileName);
}

float MasterControl::Sine(float freq, float min, float max, float shift)
{
    if (min > max)
        Swap(min, max);

    const float phase{ M_PI * freq * world_.scene_->GetElapsedTime() + shift };
    const float add{ 0.5f * (min + max) };

    return Sin(phase * M_RADTODEG) * 0.5f * (max - min) + add;
}

float MasterControl::Cosine(float freq, float min, float max, float shift)
{
    if (min > max)
        Swap(min, max);

    const float phase{ M_PI * freq * world_.scene_->GetElapsedTime() + shift };
    const float add{ 0.5f * (min + max) };

    return Cos(phase * M_RADTODEG) * 0.5f * (max - min) + add;
}
