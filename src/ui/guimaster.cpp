/* Quatter
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../quattercam.h"
#include "../effectmaster.h"

#include "guimaster.h"

GUIMaster::GUIMaster(Context* context): Master(context),
    faces_{},
    banners_{},
    logoSprite_{},
    activeSide_{ 0 }
{
    Init3DGUI();
    CreateLogo();
    CreateBanners();

    SubscribeToEvent(E_UPDATE, DRY_HANDLER(GUIMaster, HandleUpdate));
}

void GUIMaster::Init3DGUI()
{
    Node* tableNode_{ MC->world_.tableNode_ };

    for (int s{ 0 }; s < 4; ++s)
    {
        faces_.Push(tableNode_->CreateChild("MenuPlane"));

        faces_[s]->SetPosition(Quaternion{ -90.0f * (s + 2), Vector3::UP } * Vector3{ 0.0f, -13.0f, 9.5f });
        faces_[s]->SetRotation(Quaternion{ -90.0f * (s + 2), Vector3::UP } * Quaternion{ 180.0f, Vector3::FORWARD } * Quaternion{ 90.0f, Vector3::RIGHT });
        faces_[s]->SetScale({ 16.0f, 0.1f, 16.0f });
        faces_[s]->CreateComponent<StaticModel>()->SetModel(RES_MDL("Plane"));

        UIComponent* uiComponent{ faces_[s]->CreateComponent<UIComponent>() };
        uiComponent->GetMaterial()->SetTechnique(0, CACHE->GetResource<Technique>("Techniques/DiffAdd.xml"));
        UIElement* uiRoot{ uiComponent->GetRoot() };
        uiRoot->SetSize(2048, 2048);
        uiRoot->SetLayout(LM_FREE);

        switch (s)
        {
        case 0: CreateMainMenu(uiRoot);
        break;
        default: {}
            break;
        }
    }
}

void GUIMaster::CreateMainMenu(UIElement* uiRoot)
{
    for (int b{ 0 }; b < 5; ++b) {

        Button* button{ uiRoot->CreateChild<Button>() };

        String name{};
        switch (b) {
        case 0:
            name = "Multi";
            break;
        case 1:
            name = "Local";
            break;
        case 2:
            name = "Settings";
            break;
        case 3:
            name = "Exit";
            break;
        case 4:
            name = "AI";
            break;
        default:
            break;
        }

        const int mipFactor{ 3 - static_cast<int>(MC->GetGraphicsQuality()) };
        button->SetName(name);
        button->SetTexture(CACHE->GetResource<Texture2D>("Textures/UI/" + name + ".dds"));
        button->SetAlignment(HA_CENTER, VA_CENTER);
        button->SetPosition(VectorFloorToInt(Vector2{ 1.0f, 0.8f } * LucKey::Rotate(Vector2::LEFT * uiRoot->GetHeight() * .34f, b * 360.f / 5)));
        button->SetSize(IntVector2{ button->GetTexture()->GetWidth(), button->GetTexture()->GetHeight() } * mipFactor);
        if (b == 0 || b == 2)
        {
            button->SetColor(Color::RED.Transparent(.23f));
            button->SetEnabled(false);
        }
        else
        {
            button->SetColor(C_TOPLEFT, COLOR_GLOW);
        }

        SubscribeToEvent(button, E_CLICK, DRY_HANDLER(GUIMaster, HandleClick));
    }
}

void GUIMaster::CreateLogo()
{
    Texture2D* logoTexture{ CACHE->GetResource<Texture2D>("Textures/Logo.png") };

    if (!logoTexture)
        return;

    logoSprite_ = GUI->GetRoot()->CreateChild<Sprite>();

    // Set logo sprite texture
    logoSprite_->SetTexture(logoTexture);

    int textureWidth{ 1024 };
    int textureHeight{ 512 };

    logoSprite_->SetScale(Min(1.0f, (GRAPHICS->GetHeight() / static_cast<float>(textureHeight)) * .42f));
    logoSprite_->SetSize(textureWidth, textureHeight);
    logoSprite_->SetHotSpot(textureWidth / 2, 0);
    logoSprite_->SetAlignment(HA_CENTER, VA_TOP);
    logoSprite_->SetOpacity(.99f);
    logoSprite_->SetPriority(-100);
}

void GUIMaster::CreateBanners()
{
    for (bool right: { false, true })
    {
        BorderImage* banner{ GUI->GetRoot()->CreateChild<BorderImage>() };
        banner->SetBlendMode(BLEND_ALPHA);
        banner->SetSize(VectorRoundToInt(IntVector2{ 1024, 2048 } * (GRAPHICS->GetHeight() / 5e3f)));
        banner->SetTexture(RES(Texture2D, "Textures/UI/Banners.png"));
        banner->SetImageRect({ 0 + 1024 * right, 0, 1024 + 1024 * right, 2048 });
        banner->SetHorizontalAlignment(right ? HA_RIGHT : HA_LEFT);
        banner->SetOpacity(0.f);
        banners_.Push(banner);
    }

    ShowPlayerBanner(0u);
}

void GUIMaster::SetLogoVisible(bool enable)
{
    logoSprite_->SetVisible(enable);
    INPUT->SetMouseVisible(enable);

    for (Node* face: faces_)
        face->SetEnabled(enable);
}

void GUIMaster::ShowPlayerBanner(unsigned player)
{
    for (unsigned  p{ 0u }; p < banners_.Size(); ++p)
        GetSubsystem<EffectMaster>()->Fade(banners_.At(p), p + 1u == player, .05f);
}

void GUIMaster::HandleUpdate(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    for (Node* node: faces_)
    {
        UIComponent* uiComponent{ node->GetComponent<UIComponent>() };
        UIElement* uiRoot{ uiComponent->GetRoot() };

        for (SharedPtr<UIElement> el: uiRoot->GetChildren())
        {
            if (el->IsEnabled())
                for (int c{ 0 }; c < MAX_UIELEMENT_CORNERS; ++c)
                    el->SetColor(static_cast<Corner>(c), (el->GetColor(static_cast<Corner>(c)) + COLOR_GLOW * Random(.23f, 2.3f)) * .5f);
        }
    }
}

void GUIMaster::HandleClick(StringHash /*eventType*/, VariantMap& eventData)
{
    UIElement* element{ static_cast<UIElement*>(eventData[Click::P_ELEMENT].GetPtr()) };

    if (!element)
        return;

    if (activeSide_ == 0)
    {
        if (element->GetName() == "Exit")
        {
            MC->Exit();
        }
        else if (element->GetName() == "Settings")
        {
            //        activeSide_ = 1;
        }
        else if (element->GetName() == "Multi")
        {
            //        activeSide_ = 3;
        }
        else if (element->GetName() == "Local" || element->GetName() == "AI")
        {
            MC->SetEnableAI(element->GetName() == "AI");
            MC->world_.camera_->TargetBoard();
            MC->Reset();
        }
    }
}
