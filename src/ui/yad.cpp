/* Quatter
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "yad.h"
#include "../effectmaster.h"

void Yad::RegisterObject(Context *context)
{
    context->RegisterFactory<Yad>();
}

Yad::Yad(Context* context): LogicComponent(context),
    model_{},
    material_{},
    light_{},
    hidden_{ true },
    dimmed_{ false }
{
    SetUpdateEventMask(USE_NO_EVENT);
    SubscribeToEvent(E_GRAPHICSQUALITYCHANGED, DRY_HANDLER(Yad, HandleGraphicsQualityChanged));
}

void Yad::Start()
{
    node_->AddTag("Yad");

    model_ = node_->CreateComponent<AnimatedModel>();
    model_->SetModel(MC->GetModel("Yad"));
    material_ = MC->GetMaterial("Glow")->Clone();
    model_->SetMaterial(material_);

    Node* lightNode{ node_->CreateChild("Light", LOCAL) };
    lightNode->SetPosition(Vector3::UP * 0.23f);
    light_ = lightNode->CreateComponent<Light>();
    light_->SetLightType(LIGHT_POINT);
    light_->SetColor(COLOR_GLOW);
    light_->SetRange(1.0f);
    light_->SetBrightness(YAD_FULLBRIGHT);

    UpdateGraphics();
    Hide();
}

void Yad::UpdateGraphics()
{
    const GraphicsQuality quality{ MC->GetGraphicsQuality() };
    light_->SetEnabled(quality != QUALITY_LOW);
    light_->SetCastShadows(quality == QUALITY_HIGH);
}

void Yad::Dim()
{
    FX->FadeTo(light_, YAD_DIMMED);
}

void Yad::Hide()
{
    hidden_ = true;
    FX->FadeOut(light_);
    FX->FadeOut(material_, 0.1f);
}

void Yad::Reveal()
{
    hidden_ = false;
    Restore();
}

void Yad::Restore()
{
    FX->FadeTo(light_, YAD_FULLBRIGHT);
    FX->FadeTo(material_, COLOR_GLOW, 0.1f);
}
