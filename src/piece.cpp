/* Quatter
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "quattercam.h"
#include "effectmaster.h"
#include "board.h"
#include "piece.h"

void Piece::RegisterObject(Context *context)
{
    context->RegisterFactory<Piece>();
}

Piece::Piece(Context* context): Component(context),
    attributes_{},
    state_{ PieceState::FREE }
{
    SubscribeToEvent(E_GRAPHICSQUALITYCHANGED, DRY_HANDLER(Piece, HandleGraphicsQualityChanged));
}

void Piece::OnNodeSet(Node* node)
{
    if (!node_)
        return;

    node_->SetRotation(Quaternion{ Random(360.f), Vector3::UP });
    node_->AddTag("Piece");

    pieceModel_ = node_->CreateComponent<StaticModel>();
    pieceModel_->SetCastShadows(true);

    outlineModel_ = node_->CreateComponent<StaticModel>();
    outlineModel_->SetCastShadows(false);

    Node* lightNode{ node_->CreateChild("Light", LOCAL) };
    lightNode->SetPosition(Vector3::UP * .5f);
    light_ = lightNode->CreateComponent<Light>();
    light_->SetColor(Color(.0f, .8f, .5f));
    light_->SetBrightness(.0f);
    light_->SetRange(3.f);
}

void Piece::Init(PieceAttributes attributes)
{
    attributes_ = attributes;

    UpdateGraphics();

    outlineModel_->SetEnabled(false);
}

void Piece::UpdateGraphics()
{
    String postfix{};
    if (MC->GetGraphicsQuality() == QUALITY_LOW)
        postfix = "_LOQ";
    else if (MC->GetGraphicsQuality() == QUALITY_HIGH)
        postfix = "_HQ";

    pieceModel_->SetModel(MC->GetModel("Piece_" + GetCodon(3) + postfix));

    if (attributes_[COLOR])
        pieceModel_->SetMaterial(MC->GetMaterial("WoodLight"));
    else
        pieceModel_->SetMaterial(MC->GetMaterial("WoodDark"));

    if (MC->GetGraphicsQuality() != QUALITY_LOW)
        postfix = "";

    outlineModel_->SetModel(MC->GetModel("Piece_" + GetCodon(2) + "_outline" + postfix));
    outlineModel_->SetMaterial(MC->GetMaterial("Glow")->Clone());
    outlineModel_->GetMaterial()->SetShaderParameter("MatDiffColor", Color::BLACK);

    if (light_)
        light_->SetEnabled(MC->GetGraphicsQuality() != QUALITY_LOW);
}

void Piece::Reset()
{
    node_->SetParent(MC->world_.scene_);

    if (MC->GetSelectedPiece() == this)
        MC->DeselectPiece();
    if (MC->GetPickedPiece() == this)
        MC->SetPickedPiece(nullptr);

    if (state_ != PieceState::FREE)
    {
        state_ = PieceState::FREE;
        FX->ArchTo(node_,
                   MC->AttributesToPosition(ToInt()),
                   Quaternion(Random(360.f), Vector3::UP),
                   (attributes_[HEIGHT] ? 2.0f : 1.3f) +
                   (attributes_[SHAPE]  ? 0.5f : 1.0f) +
                   Random(.23f),
                   RESET_DURATION,
                   Random(.42f) + (.23f * ToInt() / NUM_PIECES));
    }
}

String Piece::GetCodon(int length) const
{
    if (length > static_cast<int>(attributes_.size()) || length < 1)
        length = static_cast<int>(attributes_.size());

    String codon{};

        codon += attributes_[HEIGHT] ? "T" : "S"; //Tall  : Short
    if (length > 1)
        codon += attributes_[SHAPE] ? "R" : "S"; //Round : Square
    if (length > 2)
        codon += attributes_[HOLE] ? "H" : "S"; //Hole  : Solid
    if (length == NUM_ATTRIBUTES)
        codon += attributes_[COLOR] ? "L" : "D"; //Light : Dark

    return codon;
}

float Piece::GetAngle() const
{
    return MC->AttributesToAngle(ToInt());
}

void Piece::Select()
{
    if ((MC->GetGameState() == GameState::PLAYER1PICKS ||
         MC->GetGameState() == GameState::PLAYER2PICKS))
    {
        outlineModel_->SetEnabled(true);

        if (state_ == PieceState::FREE) {

            state_ = PieceState::SELECTED;
            FX->FadeTo(outlineModel_->GetMaterial(), COLOR_GLOW);

            if (light_ && light_->IsEnabled())
                FX->FadeTo(light_, 0.666f);
        }
    }
}

void Piece::Deselect()
{
    if (state_ == PieceState::SELECTED)
    {
        state_ = PieceState::FREE;
        FX->FadeOut(outlineModel_->GetMaterial());

        if (light_ && light_->IsEnabled())
            FX->FadeOut(light_);
    }
}

bool Piece::Pick()
{
    if (state_ != PieceState::PUT)
    {
        state_ = PieceState::PICKED;
        MC->SetPickedPiece(this);

        if (MC->GetGameState() == GameState::PLAYER1PICKS)
            node_->SetParent(CAMERA->GetPocket(false));
        if (MC->GetGameState() == GameState::PLAYER2PICKS)
            node_->SetParent(CAMERA->GetPocket(true));

        FX->ArchTo(node_, Vector3::DOWN, Quaternion(10.0f, Vector3(1.0f, 0.0f, 0.5f)), 1.0f, 0.8f);
        FX->FadeOut(outlineModel_->GetMaterial());

        if (light_ && light_->IsEnabled())
            FX->FadeOut(light_, 0.05f);

        MC->NextPhase();
        return true;
    }

    return false;
}

void Piece::Put(Vector3 position)
{
    if (state_ == PieceState::PICKED) {

        state_ = PieceState::PUT;
        MC->SetPickedPiece(nullptr);
        node_->SetParent(MC->world_.scene_);
        FX->ArchTo(node_, position, Quaternion(Random(-13.0f, 13.0f), Vector3::UP), 2.3f, 0.5f);
    }
}
