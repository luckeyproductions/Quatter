/* Quatter
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "ai.h"
#include "piece.h"

AI::AI(Context* context): Object(context),
    personality_{ RandomPersonality(), RandomPersonality() },
    delay_{ -M_INFINITY },
    composition_{}
{
}

void AI::Reset(bool demo)
{
    delay_ = Max(.125f, delay_) * Random(2.3f, 3.4f);

    if (demo)
        delay_ *= .23f;

    UpdateComposition();
}

void AI::TakeTurn(float timeStep)
{
    if (delay_ < 0.f)
    {
        const GameMode gm{ MC->GetGameMode() };
        if (!(gm == GM_CVC && MC->GameOver()))
        {
            const Personality personality{ GetCurrentPersonality() };
            delay_ = Random(.23f, .42f - .05f * personality);

            switch (personality)
            {
            case AP_DICEY: default: DiceyTurn();    break;
            case AP_SAFE:           SafeTurn();     break;
            case AP_WIN:            WinTurn();      break;
            case AP_SAFEWIN:        SafeWinTurn();  break;
            }
        }
        else if (delay_ < IDLE_THRESHOLD * -2.3f)
        {
            delay_ = 1.f;
            MC->Reset();
        }
    }

    delay_ -= timeStep;
}

Personality AI::GetCurrentPersonality()
{
    return (MC->InPlayer1State()
            ? personality_.first_
            : personality_.second_);
}

void AI::DiceyTurn()
{
    if (MC->InPickState())
    {
        if (!MC->GetSelectedPiece())
        {
            for (int s{ Random(16) }; s > 0; --s)
                MC->StepSelectPiece(true);
        }
        else if (Random(5))
        {
            MC->StepSelectPiece(RandomBool());
        }
        else if (MC->GetSelectedPiece())
        {
            MC->GetSelectedPiece()->Pick();
            delay_ *= 2.f;
        }
    }
    else if (MC->InPutState())
    {
        if (!BOARD->GetSelectedSquare() || Random(2))
            BOARD->SelectNearestSquare({ RandomOffCenter(1.5f), 0.f, RandomOffCenter(1.5f) });
        else if (Random(3))
            BOARD->Step({ RandomSign(), RandomSign() });
        else if (BOARD->GetNearestSquare(BOARD->GetSelectedSquare()->GetNode()->GetWorldPosition()) != BOARD->GetSelectedSquare())
            BOARD->SelectNearestFreeSquare(BOARD->GetSelectedSquare()->GetNode()->GetWorldPosition());
        else
        {
            BOARD->PutPiece(MC->GetPickedPiece(), BOARD->GetSelectedSquare());
            delay_ *= 3.f;
        }
    }
}

void AI::SafeTurn()
{
    if (MC->InPutState())
        DiceyTurn();

    Vector<int> safePieces{ FindSafePieces() };
    if (safePieces.IsEmpty() || safePieces.Size() == MC->GetFreePieces().Size())
        DiceyTurn();
    else
//        MC->SelectPiece(Random(safePieces)); Pick
        DiceyTurn();

}

void AI::WinTurn()
{
    if (MC->InPickState())
        DiceyTurn();

    Vector<IntVector2> winSquares{ FindWinningMoves() };
    if (winSquares.IsEmpty() /*|| winSquares.Size() == BOARD->GetFreeSquares().Size()*/)
        DiceyTurn();
    else
//        MC->SelectSquare(Random(winSquares)); Put
        DiceyTurn();
}

void AI::SafeWinTurn()
{
    if (MC->InPickState())
        SafeTurn();
    else if (MC->InPutState())
        WinTurn();
}

Vector<IntVector2> AI::FindWinningMoves(int piece)
{
    Vector<IntVector2> winningSquares{};

    if (piece < 0)
        piece = MC->GetPickedPiece()->ToInt();

    // Check rows
    // Check columns
    // Check diagonal
    // Check 2x2

    return winningSquares;
}

Vector<int> AI::FindSafePieces()
{
    Vector<int> safePieces{};
    for (Piece* piece: MC->GetFreePieces())
    {
        const int p{ piece->ToInt() };
        if (FindWinningMoves(p).IsEmpty())
            safePieces.Push(p);
    }

    return safePieces;
}
