#!/bin/sh

./.installreq.sh
./.builddry.sh

git pull
qmake Quatter.pro -o ../Quatter-build/; cd ../Quatter-build
sudo make install; cd -; ./.postinstall.sh
rm -rf ../Quatter-build
